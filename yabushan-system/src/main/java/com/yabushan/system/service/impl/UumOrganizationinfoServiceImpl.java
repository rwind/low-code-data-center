package com.yabushan.system.service.impl;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;

import com.yabushan.common.core.domain.entity.SysDept;
import com.yabushan.common.utils.DateUtils;
import com.yabushan.common.utils.SecurityUtils;
import com.yabushan.common.utils.StringUtils;
import com.yabushan.system.utils.OrgTreeSelect;
import com.yabushan.system.utils.UumOrgInfo;
import io.netty.channel.ChannelId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.yabushan.system.mapper.UumOrganizationinfoMapper;
import com.yabushan.system.domain.UumOrganizationinfo;
import com.yabushan.system.service.IUumOrganizationinfoService;

/**
 * 租户组织Service业务层处理
 *
 * @author yabushan
 * @date 2021-04-17
 */
@Service
public class UumOrganizationinfoServiceImpl implements IUumOrganizationinfoService
{
    @Autowired
    private UumOrganizationinfoMapper uumOrganizationinfoMapper;

    /**
     * 查询租户组织
     *
     * @param ouguid 租户组织ID
     * @return 租户组织
     */
    @Override
    public UumOrganizationinfo selectUumOrganizationinfoById(String ouguid)
    {
        return uumOrganizationinfoMapper.selectUumOrganizationinfoById(ouguid);
    }

    /**
     * 查询租户组织列表
     *
     * @param uumOrganizationinfo 租户组织
     * @return 租户组织
     */
    @Override
    public List<UumOrganizationinfo> selectUumOrganizationinfoList(UumOrganizationinfo uumOrganizationinfo)
    {
        return uumOrganizationinfoMapper.selectUumOrganizationinfoList(uumOrganizationinfo);
    }

    /**
     * 新增租户组织
     *
     * @param uumOrganizationinfo 租户组织
     * @return 结果
     */
    @Override
    public int insertUumOrganizationinfo(UumOrganizationinfo uumOrganizationinfo)
    {
        String orgId =UUID.randomUUID().toString();
        //获取到父亲节点的信息
        UumOrganizationinfo parentOrg = selectUumOrganizationinfoById(uumOrganizationinfo.getParentOuguid());
        parentOrg.setParentOuguid(parentOrg.getOuguid());
        parentOrg.setOrgState(uumOrganizationinfo.getOrgState());
        parentOrg.setOuName(uumOrganizationinfo.getOuName());
        parentOrg.setOuFullName(parentOrg.getOuFullName()+"-"+uumOrganizationinfo.getOuName());
        parentOrg.setOuOrder(uumOrganizationinfo.getOuOrder());
        parentOrg.setOrgTypeId(uumOrganizationinfo.getOrgTypeId());
        parentOrg.setOrgStartTime(uumOrganizationinfo.getOrgStartTime());
        //封装子节点信息
        if(parentOrg.getOuLevel()==1){
            //公司
            parentOrg.setCompanyId(orgId);
            parentOrg.setCompanyName(uumOrganizationinfo.getOuName());
        }else if(parentOrg.getOuLevel()==2){
            //分公司
            parentOrg.setBranchId(orgId);
            parentOrg.setBranchName(uumOrganizationinfo.getOuName());
        }else if(parentOrg.getOuLevel()==3){
            //部门
            parentOrg.setDepartmentId(orgId);
            parentOrg.setDepartmentName(uumOrganizationinfo.getOuName());
        }else if(parentOrg.getOuLevel()==4){
            //科室
            parentOrg.setOfficeId(orgId);
            parentOrg.setOfficeName(uumOrganizationinfo.getOuName());
        }else if(parentOrg.getOuLevel()==5){
            //团队
            parentOrg.setTeamId(orgId);
            parentOrg.setTeamName(uumOrganizationinfo.getOuName());
        }else if(parentOrg.getOuLevel()==6){
            //小组
            parentOrg.setGroupId(orgId);
            parentOrg.setGroupName(uumOrganizationinfo.getOuName());
        }
        parentOrg.setOuLevel(parentOrg.getOuLevel()+1);
        //上级组织负责人
        parentOrg.setOrgSupervisor(parentOrg.getOrgMainLeader());
        parentOrg.setOrgMainLeader(uumOrganizationinfo.getOrgMainLeader());
        parentOrg.setOrgOtherLeader(uumOrganizationinfo.getOrgOtherLeader());
        parentOrg.setOrgParentCode(parentOrg.getOrgCode());
        parentOrg.setOrgCode(uumOrganizationinfo.getOrgCode());
        parentOrg.setOuguid(orgId);
        parentOrg.setCreateTime(DateUtils.getNowDate());
        parentOrg.setUpdateTime(DateUtils.getNowDate());
        parentOrg.setCreateBy(SecurityUtils.getUsername());
        parentOrg.setUpdateBy(SecurityUtils.getUsername());
        return uumOrganizationinfoMapper.insertUumOrganizationinfo(parentOrg);
    }

    /**
     * 修改租户组织
     *
     * @param uumOrganizationinfo 租户组织
     * @return 结果
     */
    @Override
    public int updateUumOrganizationinfo(UumOrganizationinfo uumOrganizationinfo)
    {
        //获取到父亲节点的信息
        UumOrganizationinfo org = selectUumOrganizationinfoById(uumOrganizationinfo.getOuguid());
        UumOrganizationinfo parentOrg = selectUumOrganizationinfoById(org.getParentOuguid());
        org.setOrgState(uumOrganizationinfo.getOrgState());
        org.setOuName(uumOrganizationinfo.getOuName());
        org.setOuFullName(parentOrg.getOuFullName()+"-"+uumOrganizationinfo.getOuName());
        org.setOuOrder(uumOrganizationinfo.getOuOrder());
        org.setOrgTypeId(uumOrganizationinfo.getOrgTypeId());
        org.setOrgStartTime(uumOrganizationinfo.getOrgStartTime());

        //封装子节点信息
        if(org.getOuLevel()==2){
            //公司
            org.setCompanyId(org.getOuguid());
            org.setCompanyName(uumOrganizationinfo.getOuName());
        }else if(org.getOuLevel()==3){
            //分公司
            org.setBranchId(org.getOuguid());
            org.setBranchName(uumOrganizationinfo.getOuName());
        }else if(org.getOuLevel()==4){
            //部门
            org.setDepartmentId(org.getOuguid());
            org.setDepartmentName(uumOrganizationinfo.getOuName());
        }else if(org.getOuLevel()==5){
            //科室
            org.setOfficeId(org.getOuguid());
            org.setOfficeName(uumOrganizationinfo.getOuName());
        }else if(org.getOuLevel()==6){
            //团队
            org.setTeamId(org.getOuguid());
            org.setTeamName(uumOrganizationinfo.getOuName());
        }else if(org.getOuLevel()==7){
            //小组
            org.setGroupId(org.getOuguid());
            org.setGroupName(uumOrganizationinfo.getOuName());
        }
        //上级组织负责人
        org.setOrgSupervisor(parentOrg.getOrgMainLeader());
        org.setOrgMainLeader(uumOrganizationinfo.getOrgMainLeader());
        org.setOrgOtherLeader(uumOrganizationinfo.getOrgOtherLeader());
        org.setOrgParentCode(parentOrg.getOrgCode());
        org.setOrgCode(uumOrganizationinfo.getOrgCode());
        org.setUpdateTime(DateUtils.getNowDate());
        org.setUpdateBy(SecurityUtils.getUsername());
        return uumOrganizationinfoMapper.updateUumOrganizationinfo(org);
    }

    /**
     * 批量删除租户组织
     *
     * @param ouguids 需要删除的租户组织ID
     * @return 结果
     */
    @Override
    public int deleteUumOrganizationinfoByIds(String[] ouguids)
    {
        return uumOrganizationinfoMapper.deleteUumOrganizationinfoByIds(ouguids);
    }

    /**
     * 删除租户组织信息
     *
     * @param ouguid 租户组织ID
     * @return 结果
     */
    @Override
    public int deleteUumOrganizationinfoById(String ouguid)
    {
        return uumOrganizationinfoMapper.deleteUumOrganizationinfoById(ouguid);
    }

    @Override
    public List<UumOrgInfo> selectUumOrgInfoList(UumOrgInfo uumOrgInfo) {
        return uumOrganizationinfoMapper.selectUumOrgInfoList(uumOrgInfo);
    }

    @Override
    public List<OrgTreeSelect> buildDeptTreeSelect(List<UumOrgInfo> depts) {
        List<UumOrgInfo> deptTrees = buildDeptTree(depts);
        return deptTrees.stream().map(OrgTreeSelect::new).collect(Collectors.toList());
    }

    @Override
    public List<UumOrgInfo> buildDeptTree(List<UumOrgInfo> depts) {
        List<UumOrgInfo> returnList = new ArrayList<UumOrgInfo>();
        List<UumOrgInfo> tempList = new ArrayList<UumOrgInfo>();
        UumOrgInfo rootOrg = new UumOrgInfo();
        for (UumOrgInfo dept : depts){
            if(StringUtils.isEmpty(dept.getParentOuguid())){
                rootOrg=dept;
            }else{
                tempList.add(dept);
            }
        }
        //递归遍历子节点

     /*   for (UumOrgInfo dept : depts)
        {
            tempList.add(dept.getOuguid());
        }
        for (Iterator<UumOrgInfo> iterator = depts.iterator(); iterator.hasNext();)
        {
            UumOrgInfo dept = (UumOrgInfo) iterator.next();
            // 如果是顶级节点, 遍历该父节点的所有子节点
            if (!tempList.contains(dept.getParentOuguid()))
            {
                recursionFn(depts, dept);
                returnList.add(dept);
            }
        }
        if (returnList.isEmpty())
        {
            returnList = depts;
        }*/
        UumOrgInfo childs = getChilds(tempList, rootOrg);
        returnList.add(childs);
        return returnList;
    }


    private UumOrgInfo getChilds(List<UumOrgInfo> list, UumOrgInfo root){
        List<UumOrgInfo> childrens = new ArrayList<>();

        for(UumOrgInfo child :list){
            if(child.getParentOuguid().equals(root.getOuguid())){
                childrens.add(child);
                getChilds(list, child);
            }
        }
        root.setChildren(childrens);
        return root;
    }

    /**
     * 递归列表
     */
    private void recursionFn(List<UumOrgInfo> list, UumOrgInfo t)
    {
        // 得到子节点列表
        List<UumOrgInfo> childList = getChildList(list, t);
        t.setChildren(childList);
        for (UumOrgInfo tChild : childList)
        {
            if (hasChild(list, tChild))
            {
                recursionFn(list, tChild);
            }
        }
    }

    /**
     * 得到子节点列表
     */
    private List<UumOrgInfo> getChildList(List<UumOrgInfo> list, UumOrgInfo t)
    {
        List<UumOrgInfo> tlist = new ArrayList<UumOrgInfo>();
        Iterator<UumOrgInfo> it = list.iterator();
        while (it.hasNext())
        {
            UumOrgInfo n = (UumOrgInfo) it.next();
            if (StringUtils.isNotNull(n.getParentOuguid()) && n.getParentOuguid().equals(t.getParentOuguid()))
            {
                tlist.add(n);
            }
        }
        return tlist;
    }

    /**
     * 判断是否有子节点
     */
    private boolean hasChild(List<UumOrgInfo> list, UumOrgInfo t)
    {
        return getChildList(list, t).size() > 0 ? true : false;
    }
}
