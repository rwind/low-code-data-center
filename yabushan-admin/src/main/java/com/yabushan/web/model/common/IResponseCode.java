package com.yabushan.web.model.common;

/**
 * 响应码接口
 * @author liuy
 * @date 2020/9/8 14:05
 */
public interface IResponseCode {

    Integer getCode();

    String getMessage();
}
