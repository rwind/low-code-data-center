package com.yabushan.web.controller;

import com.yabushan.common.annotation.Log;
import com.yabushan.common.core.controller.BaseController;
import com.yabushan.common.core.domain.AjaxResult;
import com.yabushan.common.core.page.TableDataInfo;
import com.yabushan.common.enums.BusinessType;
import com.yabushan.common.utils.poi.ExcelUtil;
import com.yabushan.web.model.entity.YmxCustomerInformationInfo;
import com.yabushan.web.model.entity.YmxGiftInfo;
import com.yabushan.web.model.entity.YmxInformationOptionsInfo;
import com.yabushan.web.model.entity.YmxOrderInfo;
import com.yabushan.web.service.*;
import io.swagger.annotations.Api;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Description ManagementBackgroundController
 * @Author huihui
 * @Date 2021-03-30 13:54:55
 * @Version 1.0
 */
@RequestMapping("ymx-api/management_background")
@RestController
@Api(tags = "管理后台模块")
public class ManagementBackgroundController extends BaseController {
//    @Resource
//    private ManagementBackgroundApiService backgroundApiService;
//
//    @Resource
//    private IYmxGiftInfoService ymxGiftInfoService;
//
//    @Resource
//    private IYmxInformationOptionsInfoService ymxInformationOptionsInfoService;
//
//    @Resource
//    private IYmxCustomerInformationInfoService ymxCustomerInformationInfoService;
//
//    /**
//     * 查询订单列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:orderList')")
//    @PostMapping("/orderList")
//    public TableDataInfo orderList(@RequestBody YmxOrderInfo ymxOrderInfo) {
//        startPage();
//        List<YmxOrderInfo> list = backgroundApiService.selectYmxOrderInfoList(ymxOrderInfo);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出订单列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:orderExport')")
//    @Log(title = "订单", businessType = BusinessType.EXPORT)
//    @GetMapping("/orderExport")
//    public AjaxResult orderExport(YmxOrderInfo ymxOrderInfo) {
//        List<YmxOrderInfo> list = backgroundApiService.selectYmxOrderInfoList(ymxOrderInfo);
//        ExcelUtil<YmxOrderInfo> util = new ExcelUtil<YmxOrderInfo>(YmxOrderInfo.class);
//        return util.exportExcel(list, "info");
//    }
//
//    /**
//     * 获取订单详细信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:orderGetInfo')")
//    @GetMapping(value = "/{orderId}")
//    public AjaxResult orderGetInfo(@PathVariable("orderId") String orderId) {
//        return AjaxResult.success(backgroundApiService.selectYmxOrderInfoById(orderId));
//    }
//
//    /**
//     * 新增订单
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:orderAdd')")
//    @Log(title = "订单", businessType = BusinessType.INSERT)
//    @PostMapping("orderAdd")
//    public AjaxResult orderAdd(@RequestBody YmxOrderInfo ymxOrderInfo) {
//        return toAjax(backgroundApiService.insertYmxOrderInfo(ymxOrderInfo));
//    }
//
//    /**
//     * 修改订单
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:orderEdit')")
//    @Log(title = "订单", businessType = BusinessType.UPDATE)
//    @PutMapping("orderEdit")
//    public AjaxResult orderEdit(@RequestBody YmxOrderInfo ymxOrderInfo) {
//        return toAjax(backgroundApiService.updateYmxOrderInfo(ymxOrderInfo));
//    }
//
//    /**
//     * 删除订单
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:orderRemove')")
//    @Log(title = "订单", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{orderIds}")
//    public AjaxResult orderRemove(@PathVariable String[] orderIds) {
//        return toAjax(backgroundApiService.deleteYmxOrderInfoByIds(orderIds));
//    }
//
//    /**
//     * 查询礼物列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:giftList')")
//    @PostMapping("/giftList")
//    public TableDataInfo giftList(@RequestBody YmxGiftInfo ymxGiftInfo) {
//        startPage();
//        List<YmxGiftInfo> list = ymxGiftInfoService.selectYmxGiftInfoList(ymxGiftInfo);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出礼物列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:giftExport')")
//    @Log(title = "礼物", businessType = BusinessType.EXPORT)
//    @GetMapping("/giftExport")
//    public AjaxResult giftExport(YmxGiftInfo ymxGiftInfo) {
//        List<YmxGiftInfo> list = ymxGiftInfoService.selectYmxGiftInfoList(ymxGiftInfo);
//        ExcelUtil<YmxGiftInfo> util = new ExcelUtil<YmxGiftInfo>(YmxGiftInfo.class);
//        return util.exportExcel(list, "info");
//    }
//
//    /**
//     * 获取礼物详细信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:giftGetInfo')")
//    @GetMapping(value = "/{giftId}")
//    public AjaxResult giftGetInfo(@PathVariable("giftId") String giftId) {
//        return AjaxResult.success(ymxGiftInfoService.selectYmxGiftInfoById(giftId));
//    }
//
//    /**
//     * 新增礼物
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:giftAdd')")
//    @Log(title = "礼物", businessType = BusinessType.INSERT)
//    @PostMapping("giftAdd")
//    public AjaxResult giftAdd(@RequestBody YmxGiftInfo ymxGiftInfo) {
//        return toAjax(ymxGiftInfoService.insertYmxGiftInfo(ymxGiftInfo));
//    }
//
//    /**
//     * 修改礼物
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:giftEdit')")
//    @Log(title = "礼物", businessType = BusinessType.UPDATE)
//    @PutMapping("giftEdit")
//    public AjaxResult giftEdit(@RequestBody YmxGiftInfo ymxGiftInfo) {
//        return toAjax(ymxGiftInfoService.updateYmxGiftInfo(ymxGiftInfo));
//    }
//
//    /**
//     * 删除礼物
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:giftRemove')")
//    @Log(title = "礼物", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{giftIds}")
//    public AjaxResult giftRemove(@PathVariable String[] giftIds) {
//        return toAjax(ymxGiftInfoService.deleteYmxGiftInfoByIds(giftIds));
//    }
//
//    /**
//     * 查询客户信息列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:customerList')")
//    @PostMapping("/customerList")
//    public TableDataInfo customerList(@RequestBody YmxCustomerInformationInfo ymxCustomerInformationInfo) {
//        startPage();
//        List<YmxCustomerInformationInfo> list = ymxCustomerInformationInfoService.selectYmxCustomerInformationInfoList(ymxCustomerInformationInfo);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出客户信息列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:customerExport')")
//    @Log(title = "客户信息", businessType = BusinessType.EXPORT)
//    @GetMapping("/customerExport")
//    public AjaxResult customerExport(YmxCustomerInformationInfo ymxCustomerInformationInfo) {
//        List<YmxCustomerInformationInfo> list = ymxCustomerInformationInfoService.selectYmxCustomerInformationInfoList(ymxCustomerInformationInfo);
//        ExcelUtil<YmxCustomerInformationInfo> util = new ExcelUtil<YmxCustomerInformationInfo>(YmxCustomerInformationInfo.class);
//        return util.exportExcel(list, "info");
//    }
//
//    /**
//     * 获取客户信息详细信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:customerGetInfo')")
//    @GetMapping(value = "/{customerInformationId}")
//    public AjaxResult customerGetInfo(@PathVariable("customerInformationId") String customerInformationId) {
//        return AjaxResult.success(ymxCustomerInformationInfoService.selectYmxCustomerInformationInfoById(customerInformationId));
//    }
//
//    /**
//     * 新增客户信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:customerAdd')")
//    @Log(title = "客户信息", businessType = BusinessType.INSERT)
//    @PostMapping("customerAdd")
//    public AjaxResult customerAdd(@RequestBody YmxCustomerInformationInfo ymxCustomerInformationInfo) {
//        return toAjax(ymxCustomerInformationInfoService.insertYmxCustomerInformationInfo(ymxCustomerInformationInfo));
//    }
//
//    /**
//     * 修改客户信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:customerEdit')")
//    @Log(title = "客户信息", businessType = BusinessType.UPDATE)
//    @PutMapping("customerEdit")
//    public AjaxResult customerEdit(@RequestBody YmxCustomerInformationInfo ymxCustomerInformationInfo) {
//        return toAjax(ymxCustomerInformationInfoService.updateYmxCustomerInformationInfo(ymxCustomerInformationInfo));
//    }
//
//    /**
//     * 删除客户信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:customerRemove')")
//    @Log(title = "客户信息", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{customerInformationIds}")
//    public AjaxResult customerRemove(@PathVariable String[] customerInformationIds) {
//        return toAjax(ymxCustomerInformationInfoService.deleteYmxCustomerInformationInfoByIds(customerInformationIds));
//    }
//
//    /**
//     * 查询个人信息选项列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:optionsList')")
//    @PostMapping("/list")
//    public TableDataInfo optionsList(@RequestBody YmxInformationOptionsInfo ymxInformationOptionsInfo) {
//        startPage();
//        List<YmxInformationOptionsInfo> list = ymxInformationOptionsInfoService.selectYmxInformationOptionsInfoList(ymxInformationOptionsInfo);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出个人信息选项列表
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:optionsExport')")
//    @Log(title = "个人信息选项", businessType = BusinessType.EXPORT)
//    @GetMapping("/optionsExport")
//    public AjaxResult optionsExport(YmxInformationOptionsInfo ymxInformationOptionsInfo) {
//        List<YmxInformationOptionsInfo> list = ymxInformationOptionsInfoService.selectYmxInformationOptionsInfoList(ymxInformationOptionsInfo);
//        ExcelUtil<YmxInformationOptionsInfo> util = new ExcelUtil<YmxInformationOptionsInfo>(YmxInformationOptionsInfo.class);
//        return util.exportExcel(list, "info");
//    }
//
//    /**
//     * 获取个人信息选项详细信息
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:optionsGetInfo')")
//    @GetMapping(value = "/{optionsId}")
//    public AjaxResult optionsGetInfo(@PathVariable("optionsId") String optionsId) {
//        return AjaxResult.success(ymxInformationOptionsInfoService.selectYmxInformationOptionsInfoById(optionsId));
//    }
//
//    /**
//     * 新增个人信息选项
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:optionsAdd')")
//    @Log(title = "个人信息选项", businessType = BusinessType.INSERT)
//    @PostMapping("optionsAdd")
//    public AjaxResult optionsAdd(@RequestBody YmxInformationOptionsInfo ymxInformationOptionsInfo) {
//        return toAjax(ymxInformationOptionsInfoService.insertYmxInformationOptionsInfo(ymxInformationOptionsInfo));
//    }
//
//    /**
//     * 修改个人信息选项
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:optionsEdit')")
//    @Log(title = "个人信息选项", businessType = BusinessType.UPDATE)
//    @PutMapping("optionsEdit")
//    public AjaxResult optionsEdit(@RequestBody YmxInformationOptionsInfo ymxInformationOptionsInfo) {
//        return toAjax(ymxInformationOptionsInfoService.updateYmxInformationOptionsInfo(ymxInformationOptionsInfo));
//    }
//
//    /**
//     * 删除个人信息选项
//     */
//    //@PreAuthorize("@ss.hasPermi('ymx:background:optionsRemove')")
//    @Log(title = "个人信息选项", businessType = BusinessType.DELETE)
//    @DeleteMapping("/{optionsRemove}")
//    public AjaxResult optionsRemove(@PathVariable String[] optionsIds) {
//        return toAjax(ymxInformationOptionsInfoService.deleteYmxInformationOptionsInfoByIds(optionsIds));
//    }

}
