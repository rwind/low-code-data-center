package com.yabushan.form.service;

import java.util.List;
import com.yabushan.form.domain.JimuReportDbField;

/**
 * 报表Service接口
 * 
 * @author yabushan
 * @date 2021-07-03
 */
public interface IJimuReportDbFieldService 
{
    /**
     * 查询报表
     * 
     * @param id 报表ID
     * @return 报表
     */
    public JimuReportDbField selectJimuReportDbFieldById(String id);

    /**
     * 查询报表列表
     * 
     * @param jimuReportDbField 报表
     * @return 报表集合
     */
    public List<JimuReportDbField> selectJimuReportDbFieldList(JimuReportDbField jimuReportDbField);

    /**
     * 新增报表
     * 
     * @param jimuReportDbField 报表
     * @return 结果
     */
    public int insertJimuReportDbField(JimuReportDbField jimuReportDbField);

    /**
     * 修改报表
     * 
     * @param jimuReportDbField 报表
     * @return 结果
     */
    public int updateJimuReportDbField(JimuReportDbField jimuReportDbField);

    /**
     * 批量删除报表
     * 
     * @param ids 需要删除的报表ID
     * @return 结果
     */
    public int deleteJimuReportDbFieldByIds(String[] ids);

    /**
     * 删除报表信息
     * 
     * @param id 报表ID
     * @return 结果
     */
    public int deleteJimuReportDbFieldById(String id);
}
