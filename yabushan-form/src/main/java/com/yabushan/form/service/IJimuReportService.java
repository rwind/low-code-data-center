package com.yabushan.form.service;

import java.util.List;
import com.yabushan.form.domain.JimuReport;

/**
 * 在线excel设计器Service接口
 * 
 * @author yabushan
 * @date 2021-07-03
 */
public interface IJimuReportService 
{
    /**
     * 查询在线excel设计器
     * 
     * @param id 在线excel设计器ID
     * @return 在线excel设计器
     */
    public JimuReport selectJimuReportById(String id);

    /**
     * 查询在线excel设计器列表
     * 
     * @param jimuReport 在线excel设计器
     * @return 在线excel设计器集合
     */
    public List<JimuReport> selectJimuReportList(JimuReport jimuReport);

    /**
     * 新增在线excel设计器
     * 
     * @param jimuReport 在线excel设计器
     * @return 结果
     */
    public int insertJimuReport(JimuReport jimuReport);

    /**
     * 修改在线excel设计器
     * 
     * @param jimuReport 在线excel设计器
     * @return 结果
     */
    public int updateJimuReport(JimuReport jimuReport);

    /**
     * 批量删除在线excel设计器
     * 
     * @param ids 需要删除的在线excel设计器ID
     * @return 结果
     */
    public int deleteJimuReportByIds(String[] ids);

    /**
     * 删除在线excel设计器信息
     * 
     * @param id 在线excel设计器ID
     * @return 结果
     */
    public int deleteJimuReportById(String id);
}
