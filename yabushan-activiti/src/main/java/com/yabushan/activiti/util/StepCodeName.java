package com.yabushan.activiti.util;

import org.omg.CORBA.PRIVATE_MEMBER;

public class StepCodeName {
	private String stepCode;
	private String stepName;
	public String getStepCode() {
		return stepCode;
	}
	public void setStepCode(String stepCode) {
		this.stepCode = stepCode;
	}
	public String getStepName() {
		return stepName;
	}
	public void setStepName(String stepName) {
		this.stepName = stepName;
	}



}
