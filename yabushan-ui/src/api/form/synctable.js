import request from '@/utils/request'

// 查询动态字段信息列表
export function listFileds(query) {
  return request({
    url: '/form/fileds/filedlist',
    method: 'get',
    params: query
  })
}

// 查询动态字段信息详细
export function getFileds(fId) {
  return request({
    url: '/form/fileds/getInfo',
    method: 'post',
	data: fId
  })
}

// 新增动态字段信息
export function autoAddFileds(data) {
  return request({
    url: '/form/fileds/autoAddFileds',
    method: 'post',
    data: data
  })
}

// 修改动态字段信息
export function autoUpdateFileds(data) {
  return request({
    url: '/form/fileds/autoUpdateFileds',
    method: 'post',
    data: data
  })
}

// 删除动态字段信息
export function delFileds(fId) {
  return request({
    url: '/form/fileds/' + fId,
    method: 'delete'
  })
}

// 导出动态字段信息
export function exportFileds(query) {
  return request({
    url: '/form/fileds/export',
    method: 'get',
    params: query
  })
}

// 导出动态字段信息
export function exportAutoData(query) {
  return request({
    url: '/form/fileds/exportAutoData',
    method: 'get',
    params: query
  })
}